Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users
  scope 'api' do
    scope 'v1' do
      use_doorkeeper do
        skip_controllers :authorizations, :applications,
          :authorized_applications
      end
    end
  end

  namespace 'api' do
    namespace 'v1' do
      namespace 'admin' do
      end
    end
  end
end
