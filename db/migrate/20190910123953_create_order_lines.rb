class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|
      t.references :order, foreign_key: {on_delete: :cascade}
      t.references :movie, foreign_key: true, null: false
      t.integer :quantity, null: false, default: 1
      t.decimal :price, null: false
      t.decimal :total, default: 0
      t.timestamps
    end
  end
end
