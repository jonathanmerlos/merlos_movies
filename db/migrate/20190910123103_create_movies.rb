class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title,          null: false
      t.string :description,    null: false
      t.integer :stock,          null: false, default: 0
      t.decimal :rental_price,   null: false, default: 0
      t.decimal :sale_price,     null: false, default: 0
      t.boolean :available,      null: false, default: true
      t.integer :cached_votes_up, default: 0
      t.integer :cached_votes_down, default: 0
      t.timestamps
    end
  end
end
