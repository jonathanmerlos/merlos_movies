class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.date :completed_at
      t.decimal :total, default: 0, precision: 15, scale: 2
      t.references :user, foreign_key: true
      t.string :status, null: false
      t.timestamps
    end
  end
end
